/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import impl.DaoImplEmpleado;
import java.io.IOException;
import pojo.Empleado;

/**
 *
 * @author Jadpa21
 */
public class DlgEmpleadoController {
    private DaoImplEmpleado daoEmpleado;

    public DlgEmpleadoController() {
        daoEmpleado = new DaoImplEmpleado();
    }
    
    public void save(Empleado e) throws IOException{
        daoEmpleado.save(e);
    }
}
